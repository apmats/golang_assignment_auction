package main

//Bare minimum implementation of an item object, should probably have more fields such as those commented
type Item struct {
	Id int `json:"id"`
	// Name string `json:"name"`
	// listedBy int `json:"listedBy"`
    //
    // Furthermore we could alternatively have a couple more fields like these below
    // These would have us do some more work when placing a bid and use some more space for each item we are storing, 
    // but it would allow us to do less work when trying to get the winning bid for an item as we wouldn't have to do 
    // comparisons and could just jump straight to the bid we know to be the winning
    // This would be only really worth it if getting the winning bid was an operation that we 
    // intend to run a lot
    //
    // WinningBidId int `json:"winningBidId"`
    // WinningBidValue float64 `json:"winningBidValue"`
}

//Items is an item slice
type Items []Item
