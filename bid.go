package main

//A bid should hold a value and it should link an item to a user
type Bid struct {
	Id     int     `json:"id"`
	ItemId int     `json:"itemId"`
	UserId int     `json:"userId"`
	Value  float64 `json:"value"`
}

//Bids is a bid slice
type Bids []Bid
