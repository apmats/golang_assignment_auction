package main

import (
	"fmt"
)

func main() {
    fmt.Println("Seeding storage")
	SeedStorage()
    fmt.Println("Let's get all bids on item with id 1")
	bids, _ := GetBidsOnItem(1)
	fmt.Printf("%+v\n", bids)
    fmt.Println("Let's get items that user with id 1 has bids on")
	items, _ := GetItemsWithBidFromUser(1)
	fmt.Printf("%+v\n", items)
    fmt.Println("So right now the winning bid for item with id 1 is")
    bid, _ := GetWinningBidForItem(1)
    fmt.Printf("%+v\n", bid)
    fmt.Println("But if we are to place a bid with value of 88.5 on that item")
    PlaceBid(Bid{UserId: 1, Value: 88.5, ItemId: 1})
    fmt.Println("And then check the winning bid again for that item, it should have changed:")
    bid, _ = GetWinningBidForItem(1)
    fmt.Printf("%+v\n", bid)
}
