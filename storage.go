package main

import "fmt"

//Storing data in globals as we're not using persistent data
var bids Bids
var items Items
var users Users

//Keep counts of data instances to create unique ids
var currentBidId int
var currentItemId int
var currentUserId int

//Keep in mind that this implementation isn't thread safe and weird things might happen if you attempt to treat it as such

// Give us some seed data by creating a few users and a few items
func SeedStorage() {

	for i := 0; i < 5; i++ {
		CreateUser()
		CreateItem()
	}
	PlaceBid(Bid{UserId: 1, Value: 33, ItemId: 1})
	PlaceBid(Bid{UserId: 1, Value: 44, ItemId: 1})
	PlaceBid(Bid{UserId: 2, Value: 55, ItemId: 1})
	PlaceBid(Bid{UserId: 1, Value: 66, ItemId: 2})
}

func PurgeStorage() {
	currentBidId = 0
	currentItemId = 0
	currentUserId = 0
	bids = nil
	items = nil
	users = nil
}

// Record a bid by passing a bid object containing the value, item and user IDs
func PlaceBid(bid Bid) (Bid, error) {
	for _, item := range items {
		if item.Id == bid.ItemId {
			for _, user := range users {
				if user.Id == bid.UserId {
					currentBidId++
					bid.Id = currentBidId
					bids = append(bids, bid)
					return bid, nil
				}
			}
            //user not found
            return bid, fmt.Errorf("No such user")
		}
	}
    //item not found
    return bid, fmt.Errorf("No such item")
}

// Get bids for a specific item, by the item id
func GetBidsOnItem(itemId int) (Bids, error) {
	var itemBids Bids
	for _, bid := range bids {
		if bid.ItemId == itemId {
			itemBids = append(itemBids, bid)
		}
	}
	return itemBids, nil
}

// Get all the items that a user has bids ons
func GetItemsWithBidFromUser(userId int) (Items, error) {
	var itemsWithBids Items
	itemAlreadyAdded := make(map[int]bool)
	for _, bid := range bids {
		if bid.UserId == userId {
			itemWithBid, err := GetItemById(bid.ItemId)
			if err == nil {
				//using this map to avoid adding an item multiple times to the resulting set if a user has multiple bets on it
				if !itemAlreadyAdded[itemWithBid.Id] {
					itemsWithBids = append(itemsWithBids, itemWithBid)
					itemAlreadyAdded[itemWithBid.Id] = true
				}
			} else {
				return itemsWithBids, fmt.Errorf("Couldn't get all matching items, data corrupted")
			}
		}
	}
	return itemsWithBids, nil
}

func GetWinningBidForItem(itemId int) (Bid, error) {
	winningBid := Bid{Id: -1, Value: -1, UserId: -1, ItemId: -1}
	for _, bid := range bids {
		if bid.ItemId == itemId && winningBid.Value < bid.Value {
			winningBid = bid
		}
	}
	if winningBid.Id != -1 {
		return winningBid, nil
	} else {
		return winningBid, fmt.Errorf("No bids on that item")
	}
}

// A few helper functions to get the object we want by ID
func GetItemById(itemId int) (Item, error) {
	for _, item := range items {
		if item.Id == itemId {
			return item, nil
		}
	}
	return Item{Id: -1}, fmt.Errorf("Could not find Item with Id %d", itemId)
}

func GetBidById(bidId int) (Bid, error) {
	for _, bid := range bids {
		if bid.Id == bidId {
			return bid, nil
		}
	}
	return Bid{Id: -1}, fmt.Errorf("Could not find bid with Id %d", bidId)
}

func GetUserById(userId int) (User, error) {
	for _, user := range users {
		if user.Id == userId {
			return user, nil
		}
	}
	return User{Id: -1}, fmt.Errorf("Could not find user with Id %d", userId)
}

func CreateUser() (int, error) {
	currentUserId++
	user := User{Id: currentUserId}
	users = append(users, user)
	return user.Id, nil
}

func CreateItem() (int, error) {
	currentItemId++
	item := Item{Id: currentItemId}
	items = append(items, item)
	return item.Id, nil
}
