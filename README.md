## Assignment 1

Auction System Prototype

You have been tasked with building part of a simple online auction system which will allow users to bid on items for sale.

Provide a bid tracker interface and concrete implementation with the following functionality:
* Record a user's bid on an item
* Get the current winning bid for an item
* Get all the bids for an item
* Get all the items on which a user has bid

You are not required to implement a GUI (or CLI) or persistent storage.
Please implement this in Python or Go and include test coverage.

You may use any appropriate libraries to help, but do not include the library files in your submission.


### Notes

Implemented the requested functionality by introducing a few types. Most of the code is inside the storage.go file. Tests are included in storage_test.go. Decided on using non-persistent storage, implemented as simple go variables instead of using files or some other data storage, in order to demonstrate how operations like this would be implemented in such a case, but for obvious reasons in case this were to actually run online we'd want to connect to a data storage and use the provided API instead of writing our own logic. That would also allow us to attain thread safety, as the current code is anything but thread safe (for example, we depend on the global counter increment and pushing an element to a slice to be an atomic operation, which does not hold in case we have multiple go routines going through similar parts of code).

Run demo.go for a demonstration of the requested operations or the tests to see whether they pass.