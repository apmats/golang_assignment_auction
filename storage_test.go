package main

import (
	"reflect"
	"testing"
)

func TestPlaceBid(t *testing.T) {
	var bid Bid
	PurgeStorage()
	_, err := PlaceBid(Bid{UserId: 1, Value: 33, ItemId: 1})
	if err == nil {
		t.Errorf("Expected PlaceBid to error because of invalid item and user IDs")
	}
	PurgeStorage()
	for i := 0; i < 5; i++ {
		CreateUser()
	}
	_, err = PlaceBid(Bid{UserId: 1, Value: 33, ItemId: 1})
	if err == nil {
		t.Errorf("Expected PlaceBid to error because of invalid item ID")
	}
	PurgeStorage()
	for i := 0; i < 5; i++ {
		CreateItem()
	}
	_, err = PlaceBid(Bid{UserId: 1, Value: 33, ItemId: 1})
	if err == nil {
		t.Errorf("Expected PlaceBid to error because of invalid user ID")
	}
	PurgeStorage()
	for i := 0; i < 5; i++ {
		CreateUser()
		CreateItem()
	}
	bid, err = PlaceBid(Bid{UserId: 1, Value: 33, ItemId: 1})
	if err != nil {
		t.Errorf("Expected PlaceBid not to error out with valid IDs")
	}
	if bid.Id < 0 {
		t.Errorf("Invalid bid ID returned despite not erroring out")
	}
	if checkBid, _ := GetBidById(bid.Id); !reflect.DeepEqual(bid, checkBid) {
		t.Errorf("Expected returned bid to be retrievable")
	}
}

func TestGetWinningBidForItem(t *testing.T) {
	PurgeStorage()
	for i := 0; i < 5; i++ {
		CreateUser()
		CreateItem()
	}
	_, err := PlaceBid(Bid{UserId: 1, Value: 33, ItemId: 1})
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}
	expectedWinningBid, err := PlaceBid(Bid{UserId: 2, Value: 44, ItemId: 1})
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}
	winningBid, err := GetWinningBidForItem(1)
	if err != nil {
		t.Errorf("Expected function to work properly, instead got error: %v", err.Error)
	}
	if !reflect.DeepEqual(winningBid, expectedWinningBid) {
		t.Errorf("Expected returned bid %+v to be the winning one %+v", winningBid, expectedWinningBid)
	}
}

func TestGetBidsOnItem(t *testing.T) {
	PurgeStorage()
	for i := 0; i < 5; i++ {
		CreateUser()
		CreateItem()
	}
	firstBid, err := PlaceBid(Bid{UserId: 1, Value: 33, ItemId: 1})
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}
	secondBid, err := PlaceBid(Bid{UserId: 2, Value: 44, ItemId: 1})
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}
	thirdBid, err := PlaceBid(Bid{UserId: 3, Value: 55, ItemId: 1})
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}
	_, err = PlaceBid(Bid{UserId: 3, Value: 33, ItemId: 2})
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}
	bids, err := GetBidsOnItem(1)
	if err != nil {
		t.Errorf("Expected function to work properly, instead got error: %v", err.Error)
	}
	if len(bids) != 3 {
		t.Errorf("Expected to find 3 bids but didnt")
	}
	if !isBidContained(firstBid, bids) || !isBidContained(secondBid, bids) || !isBidContained(thirdBid, bids) {
		t.Errorf("Didn't find the expected bids in the result")
	}
}

// Helper function for the above test case
func isBidContained(bid Bid, bids Bids) bool {
	for i := 0; i < len(bids); i++ {
		if reflect.DeepEqual(bid, bids[i]) {
			return true
		}
	}
	return false
}

func TestGetItemsWithBidFromUser(t *testing.T) {
	PurgeStorage()
	for i := 0; i < 5; i++ {
		CreateUser()
		CreateItem()
	}
	_, err := PlaceBid(Bid{UserId: 1, Value: 33, ItemId: 1})
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}
	_, err = PlaceBid(Bid{UserId: 1, Value: 44, ItemId: 1})
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}
	_, err = PlaceBid(Bid{UserId: 1, Value: 55, ItemId: 2})
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}
	_, err = PlaceBid(Bid{UserId: 3, Value: 33, ItemId: 3})
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}

	items, err := GetItemsWithBidFromUser(1)
	if err != nil {
		t.Errorf("Unexpected error %v", err.Error)
	}
	if len(items) != 2 {
		t.Errorf("Expected to have bids on 2 items, instead got %v", len(items))
	}
	if !containsItemWithId(1, items) || !containsItemWithId(2, items) {
		t.Errorf("Expected both items that user has bid on to be in the result set")
	}

}

// Helper function for the above test case
func containsItemWithId(itemId int, items Items) bool {
	for i := 0; i < len(items); i++ {
		if items[i].Id == itemId {
			return true
		}
	}
	return false
}
